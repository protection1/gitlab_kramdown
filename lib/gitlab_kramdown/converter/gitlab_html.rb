# frozen_string_literal: true

require 'kramdown/converter/html'
require 'nokogiri'
require 'asciidoctor-plantuml'

module GitlabKramdown
  module Converter
    # Converts a Kramdown::Document to HTML.
    #
    # This includes GitLab custom elements from GitLab Flavored Markdown syntax
    module GitlabHtml
      # Initializes a GitLab custom HTML converter with a given document
      #
      # It accepts all existing options for HTML converter with our custom ones:
      #
      # * +clickable_images+ (default: true) - whether images will have a link to itself
      #
      # @param [Kramdown::Document] root
      # @param [Hash] options
      def initialize(root, options)
        super(root, options)

        @clickable_images = options[:clickable_images] != false
      end

      # Codeblock is customized in order to implement a different output to Mermaid
      #
      # Mermaid requires `<div class="mermaid"></div>` surrounding the content in order
      # to trigger the unobtrusive JS.
      def convert_codeblock(element, opts)
        if element.options[:lang] == 'mermaid'
          %(<div class="mermaid">#{element.value}</div>\n)
        elsif element.options[:lang] == 'plantuml'
          plantuml_setup

          img_tag = Nokogiri::HTML::DocumentFragment.parse(
            Asciidoctor::PlantUml::Processor.plantuml_content(element.value, {})
          )

          %(#{img_tag}\n)
        else
          super
        end
      end

      def plantuml_setup
        Asciidoctor::PlantUml.configure do |conf|
          conf.url = 'https://plantuml.gitlab-static.net'
          conf.png_enable = true
          conf.svg_enable = false
          conf.txt_enable = false
        end
      end

      # Images can have a link to themselves when configuration allows
      #
      # We don't autolink when image is already linked to something-else
      #
      # @param [Kramdown::Element] element
      # @param [Integer] _indent
      def convert_img(element, _indent)
        return super unless @clickable_images
        return super if @stack.last.type == :a

        href = element.attr['src']

        %(<a href="#{href}" target="_blank" rel="noopener noreferrer">#{super}</a>)
      end
    end
  end
end
